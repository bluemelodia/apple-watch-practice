//
//  FlightInterfaceController.swift
//  AirAber
//
//  Created by Melanie Lislie Hsu on 12/26/15.
//  Copyright © 2015 Mic Pringle. All rights reserved.
//

import WatchKit
import Foundation

class FlightInterfaceController: WKInterfaceController {
    @IBOutlet var flightLabel: WKInterfaceLabel!
    @IBOutlet var routeLabel: WKInterfaceLabel!
    @IBOutlet var boardingLabel: WKInterfaceLabel!
    @IBOutlet var boardTimeLabel: WKInterfaceLabel!
    @IBOutlet var statusLabel: WKInterfaceLabel!
    @IBOutlet var gateLabel: WKInterfaceLabel!
    @IBOutlet var seatLabel: WKInterfaceLabel!
    
    // optional property of type Flight, declared in Flight.swift
    var flight: Flight? {
        didSet { // trigger when the property is set
            if let flight = flight { // only configure labels if we have a valid instance of Flight
                flightLabel.setText("Flight \(flight.shortNumber)")
                routeLabel.setText(flight.route)
                boardingLabel.setText("\(flight.number) Boards")
                boardTimeLabel.setText(flight.boardsAt)
                // change text color of the label to red if the flight is delayed
                if flight.onSchedule {
                    statusLabel.setText("On Time")
                } else {
                    statusLabel.setText("Delayed")
                    statusLabel.setTextColor(UIColor.redColor())
                }
                gateLabel.setText("Gate \(flight.gate)")
                seatLabel.setText("Seat \(flight.seat)")
            }
        }
    }
    
    // set flight when the controller is first shown
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        //flight = Flight.allFlights().first!
        
        // try to cast context to an instance of Flight, if success then use it to set self.flight
        // which will in turn trigger the property observer and configure the interface
        if let flight = context as? Flight {self.flight = flight}
    }
}
