//
//  ScheduleInterfaceController.swift
//  AirAber
//
//  Created by Melanie Lislie Hsu on 12/26/15.
//  Copyright © 2015 Mic Pringle. All rights reserved.
//

import WatchKit
import Foundation


class ScheduleInterfaceController: WKInterfaceController {
    @IBOutlet var flightsTable: WKInterfaceTable!
    
    var flights = Flight.allFlights() // holds all the flight info as an array of Flight instances
    
    // create an instance of the row we built in IB for each flight in flights
    // number of rows = size of the array, and the row type is the ID we set in storyboard
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        flightsTable.setNumberOfRows(flights.count, withRowType: "FlightRow")
        
        // pass an instance of Flight to each row controller in the table
        // iterate through each row in the table, asking the table for the row controller at the given index
        for index in 0..<flightsTable.numberOfRows {
            // if you successfully cast the controller you're handed back to an instance of 
            // FlightRowController, you set flight to the corresponding flight in flights
            // this will trigger the didSet observer, configuring the labels in the table row
            if let controller = flightsTable.rowControllerAtIndex(index) as? FlightRowController {
                controller.flight = flights[index]
            }
        }
    }
    
    // retrieve the appropriate flight from flights using the row index passed to this method
    // then present the flight details interface, passing flight as the context 
    // the name that you pass to presentControllerWithName(_:context:) is the id you set in the storyboard
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        let flight = flights[rowIndex]
        presentControllerWithName("Flight", context: flight)
    }
}
